package br.edu.unisep.customers.domain.usecase;

import br.edu.unisep.customers.domain.dto.CustomerDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FindAllCustomersUseCase {

    public List<CustomerDto> execute() {
        var customers = new ArrayList<CustomerDto>();
        for (int i = 1; i <= 10; i++) {
            customers.add(new CustomerDto(i, "Customer " + i, "customer_" + i + "@gmail.com"));
        }

        return customers;
    }
}
