package br.edu.unisep.customers.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CustomerDto {

    private Integer id;
    private String name;
    private String email;

}
